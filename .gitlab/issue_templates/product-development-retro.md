# Retrospective planning and execution issue


## R&D Retrospective

The purpose of our retrospective is to help our R&D team at GitLab learn and improve as much as possible from every monthly release.

Each retrospective consist of three parts:

- [Group Retrospectives](https://about.gitlab.com/handbook/engineering/management/group-retrospectives/): retrospectives held by individual groups
- [Retrospective Summary](https://about.gitlab.com/handbook/engineering/workflow/#retrospective-summary): a short pre-recorded video which summarizes the learnings across all group retrospectives
- [Retrospective Discussion](https://about.gitlab.com/handbook/engineering/workflow/#retrospective-discussion): a 25 minute live discussion diving into retrospective discussion topics]

**Timeline**

- `M-1 26th`: GitLab Bot opens [Group Retrospective](https://about.gitlab.com/handbook/engineering/management/group-retrospectives/) issue for the current milestone.
- `M, 19th`: Group Retrospectives should be held.
- `M, 24th`: Moderator opens the Retrospective planning and execution issue.
  - Moderator sends a reminder to all the main R&D channels
    - [`#development`](https://gitlab.slack.com/archives/C02PF508L)
    - [`#product`](https://gitlab.slack.com/archives/C0NFPSFA8)
    - [`#quality`](https://gitlab.slack.com/archives/C3JJET4Q6)
    - [`#ux`](https://gitlab.slack.com/archives/C03MSG8B7)
- `M, 24th` to `M+1, 3rd`: Participants complete the Retrospective planning and execution issue, add their notes to the [retro doc](https://docs.google.com/document/d/1nEkM_7Dj4bT21GJy0Ut3By76FZqCfLBmFQNVThmW2TY/edit#), and suggest and vote on discussion topics.
- `M+1, 4th`: Moderator records the Retrospective Summary video and announces the video and discussion topics.
- `M+1, 6th`: Retrospective Discussion is held.

**Moderator**

The moderator of each retrospective is responsible for:

- Coordinating the Retrospective planning and execution issue.
- Presenting the Retrospective Summary.
- Hosting the Retrospective Discussion.

The job of a moderator is to remain objective and is focused on guiding conversations forward. The moderator for each retrospective is assigned by the VP Development in each milestone.

**Retrospective planning and execution issue**

For each monthly release, a Retrospective planning and execution issue ([example](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8418)) is opened by the moderator to help us coordinate this work.

**Retro doc**

The [retro doc](https://docs.google.com/document/d/1nEkM_7Dj4bT21GJy0Ut3By76FZqCfLBmFQNVThmW2TY/edit) is a Google Doc we use to collaborate on for our Retrospective Summary and Retrospective Discussion.

### Moderator Opening Tasks

- [ ] Add a comment with `Template Improvements Thread` to track changes that need to make to [this issue template](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/.gitlab/issue_templates/product-development-retro.md)

### Group Retrospectives

At the end of every release, each group should host their own retrospective. For details on how this is done, see [Group Retrospectives](/handbook/engineering/management/group-retrospectives/).

### Retrospective Summary

The Retrospective Summary is a short pre-recorded video which summarizes the learnings across all [Group Retrospectives](/handbook/engineering/management/group-retrospectives/) ([example video](https://www.youtube.com/watch?v=XdENBhVOSiw&feature=youtu.be), [example presentation](https://docs.google.com/presentation/d/1kH9TwUAXbslM1cac938hS4Y-3mEBojQlwHw_Mm44kDE/edit?usp=sharing)).

Once all Group Retrospectives are completed, each group inputs their learnings into a single publicly-accessible [retro doc](https://docs.google.com/document/d/1ElPkZ90A8ey_iOkTvUs_ByMlwKK6NAB2VOK5835wYK0/edit?usp=sharing). The  moderator then pre-records a video of the highlights. This video is then announced in the Retrospective planning and execution issue along with the #whats-happening-at-gitlab slack channel. In line with our value of [transparency](/handbook/values/#transparency), we also post this video to our public [GitLab Unfiltered channel](https://www.youtube.com/c/GitLabUnfiltered/videos).

**Steps for participants**
1. Please host your Group Retrospective following the guidelines outlined in the [handbook](https://about.gitlab.com/handbook/engineering/management/group-retrospectives/).
1. After the Group Retrospective is complete, please choose a subset some of your most interesting learnings to share company-wide in the [retro doc](https://docs.google.com/document/d/1nEkM_7Dj4bT21GJy0Ut3By76FZqCfLBmFQNVThmW2TY/edit).  Please try to group by these by our [CREDIT values](https://about.gitlab.com/handbook/values/).
1. In the retro doc, if there is a learning that you would like to explicitely highlight, please add the text **highlight** at the beginning of the text. The moderator will highlight this along with other learnings listed in the retro doc when they create the pre-recorded video.
1. If there are improvement tasks for your group from the previous retrospective, please provide an update on them in the retro doc. They will be verbalized during the Retrospective Discussion.
1. If there are improvement tasks for your group in the current retrospective, please add them in the [retro doc](https://docs.google.com/document/d/1nEkM_7Dj4bT21GJy0Ut3By76FZqCfLBmFQNVThmW2TY/edit). They will be verbalized during the Retrospective Discussion.
1. Add a checkbox in the table of the Retrospective planning and execution issue when your Group Retrospective is complete and when the retro doc is updated.

**Steps for the moderator**
1. Please read through the Group Retrospective learnings in the [retro doc](https://docs.google.com/document/d/1nEkM_7Dj4bT21GJy0Ut3By76FZqCfLBmFQNVThmW2TY/edit).
1. Add the learnings into a slide deck and identify the highlights you would like to cover.
1. Record a video presentation summarizing the highlights.
1. Upload this video to our public [GitLab Unfiltered channel](https://www.youtube.com/c/GitLabUnfiltered/videos).
1. Announce the video and discussion topics in Retrospective planning and execution issue, the #whats-happening-at-gitlab slack channel, and the retro doc.

### Retrospective Discussion

The Retrospective Discussion is a 25 minute live discussion among participants where we deep dive into discussion topics from our Group Retrospectives ([example](https://www.youtube.com/watch?v=WP9E7RbNSPw)). In line with our value of [transparency](/handbook/values/#transparency), we livestream this meeting to YouTube and monitor chat for questions from viewers. Please check the [retro doc](https://docs.google.com/document/d/1nEkM_7Dj4bT21GJy0Ut3By76FZqCfLBmFQNVThmW2TY/edit?usp=sharing) for details on joining the livestream.

**Discussion Topics**

For each retrospective discussion, we aim to host an interactive discussion covering two discussion topics. We limit this to two topics due to the length of the meeting.

The discussion topics stem from our Group Retrospective learnings and should be applicable to the majority of participants.

Discussion topics are suggested by participants by commenting on the Retrospective planning and execution issue. Participants can vote on these topics by adding a :thumbsup: reaction. The two topics with the most :thumbsup: votes will be used as the discussion topics. If there are not enough votes or if the discussion topics are not relevant to the majority of participants, the moderator can choose other discussion topics.

**Meeting Agenda**

* Improvement tasks from the previous release (5 minutes)
* Discussion topics (14 minutes, 2 topics at 7 minutes each)
* Improvement tasks from the current release (5 minutes)
* Wrap up (1 minute)

**Steps for participants**

1. Suggest discussion topics by commenting on to the Retrospective planning and execution issue.
1. Vote on discussion topics by adding a :thumbsup: reaction. Voting closes on `M+1, 3rd`.
1. Once discussion topics are announced on `M+1, 4th`, begin adding your comments to the [retro doc](https://docs.google.com/document/d/1nEkM_7Dj4bT21GJy0Ut3By76FZqCfLBmFQNVThmW2TY/edit).
1. During the Retrospective Discussion, be prepared to verbalize any improvement tasks or commentary on the discussion topics. If you can't make the meeting and there is an item for you to verbalize, please ask someone else in your group to attend to do so.

**Steps for the moderator**

1. In the Retrospective planning and execution issue, create a thread asking participants to suggest and vote on topics.
1. Voting ends at the close of business on `M+1, 3rd`. Take note of which discussion topics have the most votes at this time. If there are not enough votes or if you deem the discussion topics as not relevant to the majority of participants, please choose other discussion topics.
1. Announce the discussion topics alongside of the Retrospective Summary video in the Retrospective planning and execution issue, the #whats-happening-at-gitlab slack channel, and the [retro doc](https://docs.google.com/document/d/1nEkM_7Dj4bT21GJy0Ut3By76FZqCfLBmFQNVThmW2TY/edit)
1. Ensure the Retrospective Discussion calendar invite is sent to participants by coordinating with the VP Development.
1. Host the Retrospective Discussion and press livestream to Youtube.
1. After the Retrospective Discussion, close the Retrospective planning and execution issue.

| Group                | Eng Manager         | Product Manager |  Retro done?        | Doc updated?       |
| ------------------- | --------------- | :---------: | :----------------: | :----------------: |
| Configure | @nicholasklick | @nagyv-gitlab  |  |  |
| Create:Code Review  | @mnohr + @andre3 | @phikai |  |  |
| Create:Editor       | @rkuba  | @ericschurter  |    |   | 
| Database            | @craig-gomes | @fzimmer  |  |  | 
| Distribution        | @mendeni          | @dorrino  |  |  : | 
| Ecosystem           | @arturoherrero + @leipert | @deuley | | | 
| Global Search       | @changzhengliu  | @JohnMcGuire  |  |  | 
| Growth:Activation  | @pcalder  | @jstava |  |  | 
| Create:Source Code  | @sean_carroll + @andr3  | @sarahwaldner  |  |  |
| Growth:Adoption    | @pcalder | @mkarampalas  |  |  |
| Growth:Conversion   | @pcalder  | @s_awezec |  |  | 
| Growth:Expansion    | @pcalder | @s_awezec  |  |  |  
| Fulfillment:Purchase| @chris_baus + @rhardarson | @tgolubeva  | | | 
| Fulfillment:License | @jameslopez + @rhardarson| @teresatison | | | 
| Fulfillment:Utilization | @csouthard + @rhardarson | @amandarueda | | |
| Geo                 | @nhxnguyen  | @fzimmer  |   |  | 
| Gitaly              | @zj-gitlab  | @mjwood  |   |  | 
| Manage:Access       | @dennis + @lmcandrew  | @ogolowinski  |   |   | 
| Manage:Optimize    | @wortschi + @djensen      | @ljlane  |   |   | 
| Manage:Compliance   | @dennis + @djensen  | @stkerr  |   |   | 
| Manage:Import       | @dennis + @lmcandrew  | @hdelalic  |   |   | 
| Memory              | @changzhengliu | @fzimmer  |  |  | 
| Monitor      | @crystalpoole | @kbychu  |  |  | 
| Package             | @dcroft | @trizzi  |  | | |
| Plan                | @donaldcook + @johnhope + @jlear | @gweaver + @cdybenko + @mjwood |  |  |
| Product Intelligence | @jeromezng | @kokeefe   |  | | |
| Release | @nicolewilliams | @kbychu  |   |  |
| Quality             | @tpazitny|  | | | 
| Secure:Composition Analysis | @gonzoyumo + @nmccorrison | @NicoleSchwartz |  | | 
| Secure:Dynamic Analysis | @sethgitlab + @nmccorrison | @derekferguson |  | | 
| Secure:Fuzz Testing | @sethgitlab + @nmccorrison | @stkerr |  | | 
| Secure:Static Analysis | @twoodham + @nmccorrison | @tmccaslin |  | | 
| Secure:Threat Insights  | @lkerr + @thiagocsf  |  @matt_wilson |  |  | 
| Sharding | @craig-gomes | @fzimmer  |  |  | 
| Protect:Container Security  | @lkerr + @thiagocsf  | @sam.white  |  |  | 
| UX                  | @vkarnes    |  |  |  | 
| Verify:Pipeline Authoring | @cheryl.li + @samdbeckham | @dhershkovitch  |  |  | 
| Verify:Pipeline Execution           | @cheryl.li + @samdbeckham | @jreporter  |  |  | 
| Verify:Runner       | @erushton | @DarrenEastman  |  |  | 
| Verify:Testing      | @shampton  | @jheimbuck_gl  |  |  | 

### Closing Tasks
 
- [ ] Resolve any open `Template Improvement Thread` items by updating the [issue template](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/.gitlab/issue_templates/product-development-retro.md))
- [ ] Close this issue after the Retrospective Livestream has occurred


cc: @fseifoddini @adawar @kencjohnston @david @joshlambert @hilaqu @meks

