--- 
title: We've started donation campaign
description: "We've started donation campaign to get support from our users. We are going to provide a better software for you with every new release."
wordpress_id: 304
wordpress_url: http://blog.gitlabhq.com/?p=304
date: 2012-03-27 12:51:34 +00:00
community: true
categories: company
---
<p>We've started donation campaign to get support from our users. We are going to provide a better software for you with every new release.  GitLab is open source project and now it needs your help.</p>


