---
layout: handbook-page-toc
title: The Procurement Team
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc}

- TOC
{:toc}


## <i class="far fa-paper-plane" id="biz-tech-icons"></i> Purchasing Policy and Team
The mission of the Procurement Team at GitLab is to be a trusted business partner at driving business value through external partnerships.

#### Why Procurement? 
- Team members can purchase goods and services on behalf of the company in accordance with the [Signature Authorization Matrix](/handbook/finance/authorization-matrix/) and guide to [Spending Company Money](/handbook/spending-company-money/). 
- However, all purchases made on behalf of GitLab that are not a personal expense, must first be reviewed by Procurement, then signed off by key members of the leadership team. 
- Approved Purchase Request Issues are required for all third party purchases being made on behalf of GitLab that aren't considered a [personal reimbursable expense](/handbook/spending-company-money/#expense-policy). 
- This ensures the organization can appropriately plan for spend and assess supplier risk. 

**Exceptions to this are:**
1.  Last minute un-planned onsite event needs such as food and rental transactions needed for event.
2.  One-time field marketing and event purchases less than $10K such as booth rentals, AV equipment, catering, etc.
     - In this instance, the vendor can invoice GitLab and AP will route approvals based on the matrix.

#### How do I contact Procurement?
- If you have a general question or are looking for direction, use the #procurement slack channel.
- Attend the Procurement Office Hours available on the GitLab Team Calendar.
- Create an Intake Form based on your needs (Marketing, Software or General/Professional Svcs Intake Form) - for **US** or **Netherlands entity**
- Create a GitLab Procurement Issue based on your needs (Marketing, Software or General/Professional Svcs Intake Form) - for **Non-US** or **Non Netherlands entity**


#### When should I contact Procurement?
- See the purchase request process for when to contact Procurement and how to open a purchase request issue.

## <i class="fas fa-stream" id="biz-tech-icons"></i> Procurement toolkit
<div class="flex-row" markdown="0" style="height:110px;">
  <a href="/handbook/finance/procurement/vendor-selection-process/" class="btn cta-btn ghost-purple" style="width:250px;margin:5px;display:flex;align-items:center;height:100%;"><span>Vendor selection process</span></a>  
  <a href="/handbook/finance/procurement/purchase-request-process/" class="btn cta-btn ghost-purple" style="width:250px;margin:5px;display:flex;align-items:center;height:100%;"><span>Purchase request process</span></a>
  <a href="/handbook/finance/procurement/vendor-guidelines/" class="btn cta-btn ghost-purple" style="width:250px;margin:5px;display:flex;align-items:center;height:100%;"><span style="margin-left: auto; margin-right: auto;">Vendor guidelines</span></a>
</div>

## How to start Procurement process?
1. Identify the Billing Entity of your purchase.
  - **US** or **Netherlands entity**: GitLab Inc, GitLab Federal LLC, GitLab BV, GitLab IT BV
  <br> OR <br>
  - **Non-US** or **Non-Netherlands entity**: GitLab Gmbh, Gitlab Ltd., GitLab PTY Ltd., GitLab Canada Corp., GitLab GK, GitLab Korea Limited.
2. Identify the cost of your purchase.

- If your purchase falls under a **Non-US** or **Non-Netherlands entity**, please open a GitLab Procurement Issue based on your purchase type - Marketing, Software, Professional Services.
- If your purchase falls under a **US** or **Netherlands entity**, please submit a Coupa request following the below Procurement Guide.


## <i class="far fa-flag" id="biz-tech-icons"></i> Purchase Request Quick Guide

If your purchase request is for one of the US or Netherlands entities (_GitLab Inc, GitLab Federal LLC, GitLab BV, GitLab IT BV_), follow the instructions on the table below.

**Unsure if your vendor is new or existing?**  
Check the section [How to Search for a Supplier in Coupa](/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-search-for-a-supplier) for a detailed guide.  
If you would prefer to have assistance setting up a new vendor, please ping us in the _#procurement_ channel - indicate you are requesting new vendor setup support and identify the vendor name. A member of the Procurement Team will respond with next steps.
{: .alert .alert-info}

|   | <$25K | >$25K  | 
| ------ | ------ | ------ | 
| **New Software** | - Create a [“New Supplier” form](/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-request-a-new-supplier) in Coupa<br>- Create a [Coupa Requisition](/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-create-a-requisition) | - Create an issue with the [Intake for Software](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/issues/new?issue%5Bmilestone_id%5D=#) template | 
| **Software Renewal** | - Create a [Coupa Requisition](/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-create-a-requisition) |  - Create an issue with the [Intake for Software](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/issues/new?issue%5Bmilestone_id%5D=#) template |  
| **Add on Software** |  - Create an issue with the [Intake for Software](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/issues/new?issue%5Bmilestone_id%5D=#) template | - Create an issue with the [Intake for Software](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/issues/new?issue%5Bmilestone_id%5D=#) template |
| Subscriptions<br>**New Vendor** | - Fill out a [“New Supplier” form](/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-request-a-new-supplier) in Coupa<br>- Create a [Coupa Requisition](/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-create-a-requisition) | - Create a [“New Supplier” form](/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-request-a-new-supplier) in Coupa<br>- Create a [Coupa Requisition](/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-create-a-requisition) |
| Subscriptions<br>**Existing Vendor** | - Create a [Coupa Requisition](/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-create-a-requisition) |  Create a [Coupa Requisition](/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-create-a-requisition) | 
| Field Marketing & Events<br>**New Vendor** | - Create a [“New Supplier” form](/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-request-a-new-supplier) in Coupa<br>- Create a [Coupa Requisition](/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-create-a-requisition) | - Create a [“New Supplier” form](/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-request-a-new-supplier) in Coupa<br>- Create a [Coupa Requisition](/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-create-a-requisition) |
| Field Marketing & Events<br>**Existing Vendor** | - Create a [Coupa Requisition](/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-create-a-requisition) | - Create a [Coupa Requisition](/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-create-a-requisition) |
| Professional Services Agency/Contractor<br>**New Vendor** | - Create an issue with the [Intake Form Professional Services](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/issues/new?issue%5Bmilestone_id%5D=#) template<br>| - Create an issue with the [Intake Form Professional Services](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/issues/new?issue%5Bmilestone_id%5D=#) template<br>| 
| Professional Services Agency/Contractor<br>**Existing Vendor** | - Create a ["Professional Services Request" form](/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-complete-the-professional-services-request-form) in Coupa<br>- Create a [Coupa Requisition](/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-create-a-requisition) | - Create a ["Professional Services Request" form](/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-complete-the-professional-services-request-form) in Coupa<br>- Create a [Coupa Requisition](/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-create-a-requisition) | 
| PS Partner Team | - Create a ["Professional Services Request" form](/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-complete-the-professional-services-request-form) in Coupa<br>- Create a [Coupa Requisition](/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-create-a-requisition) | - Create a ["Professional Services Request" form](/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-complete-the-professional-services-request-form) in Coupa<br>- Create a [Coupa Requisition](/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-create-a-requisition) | 

As we implement Coupa in a [phased approach](/handbook/finance/procurement/coupa-faq/#does-the-coupa-implementation-impact-all-gitlab-entities), Purchase Requests for all other [GitLab entities](/handbook/tax/#tax-procedure-for-maintenance-of-gitlabs-corporate-structure) will continue to use the existing procurement issues.
You can learn more about Coupa in our [FAQ Page](/handbook/finance/procurement/coupa-faq/) and checking the [Guide Page](/handbook/business-technology/enterprise-applications/guides/coupa-guide/).
{: .alert .alert-info}

### How to access Coupa

Coupa is available via Okta. To access the platform:
1. Login to your [Okta home page](https://gitlab.okta.com/app/UserHome#).
1. Click on the Coupa (Prod) button.
   - A new tab should open with your user logged in.

> If your job function requires you to submit purchase requests, please submit an [Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new) for Coupa.

## <i class="fas fa-bullseye" id="biz-tech-icons"></i> Procurement Main Objectives

Procurement operates with three main goals
1.  Cost Savings
2.  Centralize Spending
3.  Compliance

## <i class="far fa-clock" id="biz-tech-icons"></i> Procurement Performance Indicators
Process of measurement in progress.

**Response times to initial requests for review <= 2 business days**
- Monthly average response time to new procurement purchase request issues within 2 business days. Issues with missing or incomplete information will be rejected after two consecutive attempts to follow up with the issue creator.

**Administer, maintain, and manage the procurement purchase request issue tracker (daily, ongoing) <= 2 business days**
* Triage and assign issues in the procurement [issue board](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/boards/1844091) within 2 business days of receipt.

**Ensure all contracts have the correct approvals in place before signed = 100%**
* Verify the correct approvers have approved all purchase requests according to the [Authorization Matrix](/handbook/finance/authorization-matrix/) before the contract is signed. 

**Ensure all fully executed vendor contracts are posted to ContractWorks = 100%**
* Upload all fully executed agreement to ContractWorks for accurate recording. Legal team will assign terms and fields.

**'Turn-Around' times <= 3 business days**
* Monthly average response times within purchase request issues for proposal updates, commercial constructs, procurement questions, etc.


## <i class="far fa-question-circle" id="biz-tech-icons"></i> Frequently asked questions

#### What if the Vendor I am working with doesn't require a contract?
1. GitLab requires a Vendor Contract even if your vendor doesn't.
1. The Vendor Contract approval process is designed to protect both you and the GitLab and the vendor. 
1. In this event legal can and will provide contract terms to govern the transaction based on the level of risk.
1. Open the appropriate vendor contract request to initiate the process

## <i class="far fa-flag" id="biz-tech-icons"></i> Important Considerations

#### Vendor Code of Ethics
All vendors that GitLab does business with, must legally comply with the [Supplier Code of Ethics](/handbook/people-policies/#partner-code-of-ethics). When having discussions with your vendor regarding the contract, make them aware of this requirement.

#### Modern Slavery and Human Trafficking Compliance Program

GitLab condemns exploitation of humans through the illegal and degrading practices of human trafficking, slavery, servitude, forced labor, forced marriage, the sale/exploitation of children and adults and debt bondage (“Modern Slavery”).  To combat such illegal activities, GitLab has implemented this Modern Slavery and Human Trafficking Compliance Program.  

##### *Risk Areas and Markets*

While Modern Slavery can occur in any country and in any market, some regions and sectors present higher likelihood of vioaltions. Geographies with higher incidents of slavery are India, China, Pakistan, Bangladesh, Uzbekistan, Russia, Nigeria, Indonesia and Egypt. Consumer sectors such as food, tobacco and clothing are high risk sectors; but Modern Slavery can occur in many markets.*  
 (*According to Source: Statista, Walk Free Foundation, https://www.statista.com/chart/4937/modern-slavery-is-a-brutal-reality-worldwide/)  

##### *Actions to Address Modern Slavery Risks*

All vendors, providers and entities providing services or products to GitLab (“Vendors”) are expected to comply with [GitLab’s Partner Code of Ethics](/handbook/people-policies/#partner-code-of-ethics) which specifically addresses Modern Slavery laws.  Compliance with the Partner Code of Ethics will be included in Vendor contracts and/or purchase orders going forward.  Existing Vendor contracts will be updated with the appropriate language upon renewal.

Those entities who are of higher risk or whom GitLab suspects may be in violation of Modern Slavery laws, may be required to complete an audit.  Audits may be presented in the form of a questionnaire or may be an onsite visit.  Any known or suspected violations will be raised to Legal and/or Compliance.  Failure to comply with Modern Slavery laws will result in a termination of the relationship and GitLab retains all rights in law and equity.

Vendors understand and agree that violations of Modern Slavery laws may require mandatory reporting to governing authorities. GitLab has discretion if and how to best consult with Vendors for purposes of Modern Slavery reporting. GitLab is sensitive to and will take into consideration, the relationship and the risk profile of Vendor to ensure that Modern Slavery risks have been appropriately identified, assessed and addressed and that the Vendor is aware of what actions it needs to take.

##### *Assessment of Effectiveness*

GitLab will review its Modern Slavery and Human Trafficking Compliance Program on an annual basis or sooner if it is determined there is increased exposure or concerns with overall compliance.   The Program may be amended from time to time by GitLab, to ensure compliance with the most current Modern Slavery laws and regulations.

##### *Compliance Program Approval*

GitLab’s Executive Team reviewed and approves this Modern Slavery and Human Trafficking Compliance Program.

## <i class="fas fa-book" id="biz-tech-icons"></i> Related Docs and Templates

##### Contract Templates

- [Mutual Non-Disclosure Agreement (NDA)](https://drive.google.com/file/d/1kQfvcnJ_G-ljZKmBnAFbphl-yFfF7W5U/view?usp=sharing)
- [Logo Authorization Template](https://drive.google.com/file/d/1Vtq3UHc8lMfIbVFJ3Mc-PZZjb6_CKAvm/view?usp=sharing)
- [Data Processing Agreement](https://drive.google.com/file/d/1zR-QYz8Hnk5XR-DaFNlzrEksL0hyFsjT/view?usp=sharing)
- [US-based Contractor Agreement](https://drive.google.com/file/d/1NIon0c92ej2CRABQsdzWbpdM-3cJy0Se/view?usp=sharing) Master agreement for a subcontractor performing internal services directly to GitLab
- [APAC Contractor Agreement](https://drive.google.com/file/d/17-PvpC63XwESBDFMq3H-ux48N9EkoHtx/view?usp=sharing) Same as above for APAC
- [MPS Subcontractor Agreement](https://drive.google.com/file/d/1MgBqa_K1oqqIR79jV3AEynbYN6Jc_V5U/view?usp=sharing) Master agreement for subcontractor performing services for the *GitLab Customer*

#### Documentation

* [Uploading Third Party Contracts to ContractWorks](/handbook/legal/vendor-contract-filing-process/)
* [Company Information](https://gitlab.com/gitlab-com/Finance-Division/finance/-/wikis/company-information) - general information about each legal entity of the company
* [Trademark](/handbook/marketing/corporate-marketing/brand-activation/brand-guidelines/#trademark) - information regarding the usage of GitLab's trademark
* [Authorization Matrix](/handbook/finance/authorization-matrix/) - the authority matrix for spending and binding the company and the process for signing legal documents
