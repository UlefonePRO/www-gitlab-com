---
layout: handbook-page-toc
title: "Labor and Employment Notices"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}
{::options parse_block_html="true" /}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

Since GitLab is an [all remote](/company/culture/all-remote/) company we don't have a physical worksite or breakroom wall upon which to post important labor and employment notices, so this page is our version of that.

<details>

<summary markdown="span">GitLab, Inc.</summary>

### California
* [California Law Prohibits Workplace Discrimination and Harassment](https://www.dfeh.ca.gov/wp-content/uploads/sites/32/2020/10/Workplace-Discrimination-Poster_ENG.pdf)
* [Transgender Rights in the Workplace](https://www.dfeh.ca.gov/wp-content/uploads/sites/32/2019/08/DFEH_TransgenderRightsWorkplace_ENG.pdf)
* [Your Rights and Obligations as a Pregnant Employee](https://www.dfeh.ca.gov/wp-content/uploads/sites/32/2020/12/Your-Rights-and-Obligations-as-a-Pregnant-Employee_ENG.pdf)
* [Family Care & Medical Leave & Pregnancy Disability Leave](https://www.dfeh.ca.gov/wp-content/uploads/sites/32/2020/12/CFRA-and-Pregnancy-Leave_ENG.pdf) 
* [Industrial Welfare Commission Wage Order](https://www.dir.ca.gov/IWC/IWCArticle17.pdf)
* [California Minimum Wage](https://www.dir.ca.gov/iwc/MW-2019.pdf)
* [Paid Sick Leave](https://www.dir.ca.gov/DLSE/Publications/Paid_Sick_Days_Poster_Template_(11_2014).pdf)
* [Safety and Health Protection on the Job](https://www.dir.ca.gov/dosh/dosh_publications/shpstreng012000.pdf)
* [Notice to Employees - Injuries Caused by Work](https://www.dir.ca.gov/dwc/NoticePoster.pdf)
* [Whistleblower Protections](https://www.dir.ca.gov/dlse/WhistleblowersNotice.pdf)
* [Notice to Employees - unemployment, disability, and paid family leave insurance](https://www.edd.ca.gov/pdf_pub_ctr/de1857a.pdf)
* [Sexual Harassment Fact Sheet](https://www.dfeh.ca.gov/wp-content/uploads/sites/32/2020/03/SexualHarassmentFactSheet_ENG.pdf)

#### City of San Francisco
* San Francisco Minimum Wage increases to $15.59 per hour [(June 2019)](ttps://gitlab.com/gitlab-com/people-ops/Compensation/uploads/e261b0f24e1b2d5bf382366bb400cf53/sf-minimum-wage-2019-06.pdf).
* [Fair Chance Ordinance](https://sfgov.org/olse/fair-chance-ordinance-fco)

### Massachusetts
* [Paid Family and Medical Leave](https://www.mass.gov/doc/paid-family-and-medical-leave-mandatory-workplace-poster/download)
* [Earned Sick Time](https://www.mass.gov/doc/earned-sick-time-notice-of-employee-rights/download)
* [Massachusetts Wage and Hours Laws](https://www.mass.gov/doc/massachusetts-wage-hour-laws-poster/download)
* [Fair Employment Law](https://www.mass.gov/doc/fair-employment-poster/download)
* [Parental Leave Act](https://www.mass.gov/service-details/parental-leave-in-massachusetts)
* [Information on Employees Unemployment Insurance Coverage](https://www.mass.gov/doc/information-on-employees-unemployment-insurance-coverage-form-2553a/download)

### New York
* [New York Correction Law, Article 23-A](https://labor.ny.gov/formsdocs/wp/correction-law-article-23a.pdf)
* [New York State Human Rights Law](https://dhr.ny.gov/sites/default/files/doc/poster.pdf)
* [Equal Pay Provision](https://labor.ny.gov/formsdocs/wp/LS603.pdf)
* [Minimum Wage](https://labor.ny.gov/formsdocs/wp/LS207.pdf)

### Pennsylvania
* [Minimum Wage Law](https://www.dli.pa.gov/Documents/Mandatory%20Postings/llc-1.pdf)
* [Abstract of Equal Pay Law](https://www.dli.pa.gov/Documents/Mandatory%20Postings/llc-8.pdf)
* [Employment Provisions of the PA Human Relations Act](https://www.phrc.pa.gov/About-Us/Publications/Documents/Required%20Posters/Fair%20Employment.pdf)

#### City of Philadelphia
* [Promoting Healthy Families and Workplaces](https://www.phila.gov/media/20191218103833/Paid-Sick-Leave-Poster-Translations.pdf)

#### City of Pittsburgh
* [Paid Sick Days Act](https://apps.pittsburghpa.gov/redtail/images/9692_Notice-Paid-Sick-Days-Act_06-2020.pdf)

### Washington
* [Paid Family and Medical Leave Act](https://paidleave.wa.gov/app/uploads/2019/12/Employer-poster.pdf)
* [Job Safety and Health Law](https://www.lni.wa.gov/forms-publications/f416-081-909.pdf)
* [Your Rights as a Worker](https://www.lni.wa.gov/forms-publications/F700-074-000.pdf)

### EEOC (U.S. Equal Employment Opportunity Commission) Notices
* ["EEO is the Law" English poster for screen readers.](https://www.eeoc.gov/sites/default/files/migrated_files/employers/poster_screen_reader_optimized.pdf)
* ["EEO is the Law" English poster for printing.](https://www.eeoc.gov/sites/default/files/migrated_files/employers/eeoc_self_print_poster.pdf)
* ["EEO is the Law" Spanish poster for printing.](https://www.eeoc.gov/sites/default/files/migrated_files/employers/eeoc_self_print_poster_spanish.pdf)
* All ["EEO is the Law"](https://www1.eeoc.gov/employers/poster.cfm) poster links.

### E-Verify 
* [Notice of E-Verify Participation Poster](https://www.e-verify.gov/sites/default/files/everify/posters/EVerifyParticipationPoster.pdf) 
* [Right to Work Poster](https://www.e-verify.gov/sites/default/files/everify/posters/IER_RighttoWorkPoster.pdf)

### Employee Polygraph Protection Act
* [EPPA Poster](https://www.dol.gov/sites/dolgov/files/WHD/legacy/files/eppac.pdf)

### Fair Labor Standards Act (FLSA) Minimum Wage 
* [Fair Labor Standards Act Poster](https://www.dol.gov/sites/dolgov/files/WHD/legacy/files/minwagep.pdf)
* [Section 14(c)](https://www.dol.gov/sites/dolgov/files/WHD/legacy/files/disabc.pdf)

### Family and Medical Leave Act 
* [Family and Medical Leave Act (FMLA) Poster](https://www.dol.gov/sites/dolgov/files/WHD/legacy/files/fmlaen.pdf)

### OSHA Job Safety and Health
* [Job Safety and Health: Its the Law Poster](https://www.osha.gov/Publications/osha3165-8514.pdf)

### Service Contract Act
* [Service Contract Act](https://www.dol.gov/sites/dolgov/files/WHD/legacy/files/govc.pdf)

</details>

<details>

<summary markdown="span">GitLab Canada Corp.</summary>

### Ontario
* [Employment Standards in Ontario](https://files.ontario.ca/mltsd-employment-standards-poster-en-2020-09-08.pdf)
* [Occupational Health & Safety Poster](https://files.ontario.ca/mltsd_2/mltsd-prevention-poster-en-2020-07-22.pdf)
* [Occupational Health & Safety Act](https://www.ontario.ca/laws/statute/90o01)

### British Columbia
* [Working in B.C.](https://www2.gov.bc.ca/assets/gov/employment-business-and-economic-development/employment-standards-workplace-safety/employment-standards/factsheets-pdfs/working_in_bc_infosheet.pdf)
* [Occupational Health and Safety Regulation](https://www.worksafebc.com/en/law-policy/occupational-health-safety/searchable-ohs-regulation/ohs-regulation)
* [Workers Compensation Act](https://www.worksafebc.com/en/law-policy/occupational-health-safety/searchable-ohs-regulation/ohs-guidelines/guidelines-for-workers-compensation-act)

### Alberta
* [Workers Compensation Act](https://www.wcb.ab.ca/assets/pdfs/employers/123_english.pdf)
* [Employment Standards Code](https://www.alberta.ca/assets/documents/es-general-online-poster.pdf)

### Manitoba 
* [Safe Work Manitoba](https://www.safemanitoba.com/Page%20Related%20Documents/resources/BR_EveryonesResponsibilityLong_15SWMB.pdf)

</details>

<details>

<summary markdown="span">GitLab BV</summary>

### Netherlands
* [European Agency for Safety and Health at Work - Netherlands](https://osha.europa.eu/en/about-eu-osha/national-focal-points/netherlands)
* [OSH - Netherlands](https://www.arboineuropa.nl/en/arbo-in-the-netherlands/)
* [Health and Safety at Work - Netherlands](https://business.gov.nl/regulation/health-safety-work/)

</details>

<details>

<summary markdown="span">GitLab GmbH</summary>

* [European Agency for Safety and Health at Work](https://osha.europa.eu/en)
* [Working Hours Act](https://www.gesetze-im-internet.de/arbzg/index.html) 

</details>
