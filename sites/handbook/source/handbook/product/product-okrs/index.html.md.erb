---
layout: handbook-page-toc
title: Product Organization OKRs
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

[**Principles**](/handbook/product/product-principles/) - [**Processes**](/handbook/product/product-processes/) - [**Categorization**](/handbook/product/categories/) - [**GitLab the Product**](/handbook/product/gitlab-the-product) - [**Being a PM**](/handbook/product/product-manager-role) - [**Performance Indicators**](/handbook/product/performance-indicators/) - [**Leadership**](/handbook/product/product-leadership/)

## What are OKRs

OKRs (Objectives and Key Results) are a simple goal setting technique propelled by Google and implemented by many leading companies with astounding success. [Five key OKR benefits] include: Focus, Alignment, Commitment, Tracking and Stretching. 

John Doerr is a leader in the OKR world and authored the book Measure What Matters, a handbook for setting and achieving audacious goals. He as since built a company called What Matters, that gives leaders the tools they need to achieve their goals. The [What Matters website](https://www.whatmatters.com/) is a rich and detailed resource for OKRs. You can also check out the [how to write OKRs](#how-to-write-okrs) section of this page for more guidance.

## Why we do Product OKRs

Product Management has the potential to drive and influence growth as well as company goal attainment in ways that other organizations in a company [cannot](https://productled.com/blog/product-led-growth-definition/).
When we consider our [Objectives and Key Results (OKRs)](https://about.gitlab.com/company/okrs/#what-are-okrs) across the Product, Engineering, UX, and Quality organizations, we can do more to make them connected and concerted. One of the [key implications for alignment across teams](https://www.forbes.com/sites/forbescommunicationscouncil/2018/12/19/three-implications-of-product-led-growth/?sh=77ccb6635fc5) is that you share goals and KPIs.
This enables us to be product-led by focusing on [Essentialism](https://about.gitlab.com/handbook/product/product-principles/#our-product-principles) to drive results that matter most for the customer across the aligned product groups. 
As it is written in the handbook, [GitLab's OKRs](https://about.gitlab.com/company/okrs/#what-are-okrs) are meant to be the "how to achieve the goal" of a KPI. So, the question we need to consider is are we optimizing our goals to achieve the KPI in the best way?

## GitLab OKRs

GitLab uses the [OKR (Objective and Key Results) framework](https://www.youtube.com/watch?v=L4N1q4RNi9I) to set and track goals on a quarterly basis. You can read more about OKRs at GitLab OKRs including the overall process as well as current and past OKRs [here](https://about.gitlab.com/company/okrs/). Every department participates in OKRs to collaboratively goal set across the organization. GitLab does quarterly objectives which align with our Fiscal Year. 

## Product OKRs

Currently, Product team OKRs are tracked as Epics [gitlab.com](https://gitlab.com/groups/gitlab-com/-/epics?scope=all&state=opened&search=FY22-Q2+OKR) and the KRs are tracked as Issues in the [Product project](https://gitlab.com/gitlab-com/Product) and on the [Product OKR Board](https://gitlab.com/gitlab-com/Product/-/boards/906048?label_name%5B%5D=OKR). Starting in FY22-Q2, all OKRs are also in [Ally.io](https://app.ally.io/teams/39480/objectives?tab=0&time_period_id=135090) so  we can get familiar with the tool before FY22-Q3. Starting in FY22-Q3, we're aiming to replace use of GitLab epics, issues and boards and fully port all OKRs into Ally.io. Details on that process and instructions on how to participate will be added to this page as they solidify. 

To learn more about the rollout process for Product and Engineering see the linked [Ally OKR Rollout GitLab Project](https://gitlab.com/gitlab-com/ally-okr-rollout).

### Product OKR Process

Each organization at GitLab may follow a slightly personalized process for OKRs. Below is the process for Product. Product works closely with Engineering and collaborates on some of the same Objectives and Key Results. You can reference the Engineering's process [here](https://about.gitlab.com/handbook/engineering/#engineering-okr-process).

The Product OKRs are initiated by the Chief Product Officer each quarter per the details and timeline below. 

#### FY22-Q2 Product OKR Process

##### OKR Kick-off by the Product Leadership Team

_Please note that the FY22-Q2 process is a "hybrid" model as we continue to leverage Gitlab epics and issues as SSOT for OKR content but shift to scoring OKRs in Ally.io.  We fully transition into Ally.io in FY22-Q3. Thank you for your patience!_

- **5 weeks** prior to the start of the fiscal quarter, the CEO and Chief of Staff initiate the OKR process. The CEO will share an MR in the #e-group Slack channel with the 3 Company Objectives for the quarter, and one to three Key Results for each objective.
- **4 weeks** prior to the start of the new quarter the Chief Product Officer will draft their OKRs in a GitLab issue in the [Product Project](https://gitlab.com/gitlab-com/Product/-/issues) and review the drafted Product OKRs with the CEO.
- **3 weeks** prior to the start of the new quarter, the CEO approves the OKRs and the Chief Product Officer shares the OKRs with the [product leadership](/handbook/product/product-leadership/) team for alignment and to finalize ownership of the KRs to specific product team members. 
- **2 weeks** prior to the start of the new quarter, the EBA to the Chief Product Officer will port the finalized OKRs into [Ally.io](https://app.ally.io/teams/39480/objectives?tab=0&time_period_id=135090) and assign the Objectives and Key Results to the designated owners, as specified by the Chief Product Officer in the finalized Gitlab epics/issues.
     - The EBA will align the Product OKRs to their corresponding parent-level OKRs so everything cascades from the Company Objectives set by the CEO and Chief of Staff.
     - The EBA will link the OKR descriptions in Ally.io to the related GitLab epics/issues for ease of reference to the final SSOT.

##### OKR Collaboration by the rest of Product Team

**Updating the status of Product OKRs**

- Team members who **own** Q2 OKRs will update the score (% complete) in the relevant GitLab epics/issues and [Key Reivew slides](https://docs.google.com/presentation/d/1xAbkJC8EAWV49bYIEyRCT7qXkiPnq4CQXX9VM_93szw/edit#slide=id.p) monthly and notify any relevant stakeholders. 
- The EBA to the Chief product Officer will then update the [Q2 scores in Ally.io](https://about.gitlab.com/company/okrs/#scoring-okrs) based on the latest Key Review slides by the end of each month. 
     - It is recommended to direct link to the monthly update in the GitLab epic/issue  or Key Review slides in the Ally.io OKR description to maintain a clear reference to the SSOT. 

#### FY22-Q3 Product OKR Process

_In FY22-Q3/Q4 we'll have an updated process to fully leverage Ally.io as SSOT for OKRs and sunset usage of GitLab epics/issues. Details forthcoming._

_During FY22 Q3, we expect many questions as we pilot Allied Product Group OKRs and the adoption of Ally.io. Any time product managers or any other team members need live support, they should add themselves to the agenda and attend [Product Operations office hours](https://about.gitlab.com/handbook/product/product-processes/#product-operations-office-hours-bi-weekly)._

##### KR Creation by Product Groups (referred to as Aligned Product Group OKRs)

**Context:** <br>
The Q3 OKR process outlined below is a follow-up to the [Q2 OKR experiment](https://gitlab.com/gitlab-com/Product/-/issues/2095) and designed to pilot **Aligned Product Group OKRs**. Aligned Product Group OKRs are drafted against GitLab's yearly product themes and focus on driving ground-up motion to inform the business and deliver value to customers. Product management will collaborate closely with the Hive: Engineering, UX and Quality, as well as Infra and Security when needed, to create Aligned Product Group OKRs. This approach aims to get ahead of the executive OKR process and provide the e-group with our opinionated view of what we plan to do. The Hive will collectively create shared key results at the Product Group level. Three key benefits of this approach:

1. Product managers serve as Aligned Product Group DRIs to ensure OKRs align with the vision of the product group
1. We further empower all Product Group [DRIs](https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/) to use existing prioritization systems to accomplish these targets, rather than this being a top-down initiative
1. The Product Group has the most context on the customer, the technology, the quality, performance, and usability of the product to come up with a goal that can be successfully delivered to move the needle for the business

We realize the introduction of Allied Product Group OKRs may initially cause some perceived or actual conflicts with other Quad members' functional team OKRs. By first doing this as an optional pilot for product groups, rather than a full roll-out, we hope to capture these challenges and work through them. Please surface any challenges to your manager, in [Product Operations office hours](https://about.gitlab.com/handbook/product/product-processes/#product-operations-office-hours-bi-weekly) or in [Feedback on Q3 Product Group Planning](https://gitlab.com/gitlab-com/Product/-/issues/2714).

**Guiding themes for shared KRs:**
- [Application Security Testing](https://about.gitlab.com/direction/#application-security-testing-leadership)
- [Adoption Through Usability](https://about.gitlab.com/direction/#adoption-through-usability)
- [SaaS First](https://about.gitlab.com/direction/#saas-first)


- **8 weeks** prior to the start of the new quarter, [the EBA to the Chief Product Officer](https://about.gitlab.com/company/team/#kristie.thomas) will create the planning Epic and Issues for Product Org and Group OKRs. These will be used as a place for teams suggest and draft their proposed OKRs in the issue comments.
  - The [EBA to the Chief Product Officer](https://about.gitlab.com/company/team/#kristie.thomas) will create [FY22-Q3 Planning Epic](https://gitlab.com/groups/gitlab-com/-/epics/1509) containing three separate issues for each 1-year product theme, and a fourth issue for OKRs not aligned with a theme. EBA will tag in [Product Operations](https://about.gitlab.com/company/team/#fseifoddini). The EBA will also create the Product Org OKR Issue which is driven by the Chief Product Officer, and nested under the above referenced FY22-Q3 Planning Epic as well.  
     - Product Operations will review and then tag all first line managers (Group Managers, or Directors) requesting they activate their product managers to propose product group KRs corresponding to one or more of the 1-year product themes as Objectives. 
     - GMPs will organize review and approval of their team's KRs at their discretion but can view suggestions [here](#how-to-write-OKRs). We strongly recommend aligning KRs to at least one of the known FY22 product themes, however, it is not a hard requirement. Please note if a team is not aligning KRs to an existing product theme as an Objective, they'll have to first define an Objective and then define the Key Results.
     - Product managers are the DRI of Aligned Product Group OKRs. As such they will own the planning, definition in partnership with their [Quad (Product Manager, Engineering Manager, Product Designer, Quality Engineer)](https://about.gitlab.com/handbook/engineering/quality/quad-planning/) and can also include their Infrastructure and Security counterparts if applicable to the objectives.  
     - Once the product group KRs are reviewed and approved by their managers, PMs will add their aligned upon KRs to the description of the corresponding 1-year product theme issue or the no-theme affiliated issue in the [Fy22-Q3 OKR Planning Epic](https://gitlab.com/groups/gitlab-com/-/epics/1509) When adding KRs to the issue description the following format should be used: 
        - Section Name
        - Group Name
        - List of Key Results
     - Product managers will link any related KR epics or issues as appropriate to the corresponding 1-year product theme issue or the no-theme affiliated issue as they become available for tracking. 




- **6 weeks** prior to the start of the new quarter, GMPs have their manager review their team's KRs from the planning issues which are nested under the [Fy22-Q3 OKR Planning Epic](https://gitlab.com/groups/gitlab-com/-/epics/1509). 
     - Product operations reviews issues/KRs and may ping GMPs and PMs with questions or reminders. At this time, Product operations will also alert the Chief Product Officer and VP Product Management that the KRs are read to be leverage for Product Organanization OKR planning. 

     **IMPORTANT NOTES:**

     - As DRI of aligned Product Group OKRs, product managers own the SSOT of the KRs, be they housed in GitLab epics/issues or in Ally.io. If duplicate drafts are found anywhere, it should be flagged in [Feedback on Q3 Product Group Planning](https://gitlab.com/gitlab-com/Product/-/issues/2714), tagging Product Operations `@fseifoddini` and Quality `@meks` for resolution.

     - For guidance on drafting clear and achievable OKRs check out: [How to write OKRs](#how-to-write-OKRs)**

##### OKR Kick-off by the Product Leadership Team

- **5 weeks** prior to the start of the fiscal quarter, the CEO and Chief of Staff initiate the OKR process. The CEO will share an MR in the #e-group Slack channel with the 3 Company Objectives for the quarter, and one to three Key Results for each objective. 

     - The Chief Product Officer and the VP of Product will review the Group Product OKRs, located in the [1-year theme OKR issues](https://gitlab.com/groups/gitlab-com/-/epics/1509) as they prepare to [draft their Q3 OKRs](https://gitlab.com/gitlab-com/Product/-/issues/2715).

-  **4 weeks** prior to the start of the new quarter the Chief Product Officer will draft their OKRs in the [Product Org FY22-Q3 Planning Issue](https://gitlab.com/gitlab-com/Product/-/issues/2715) in the [Product Project](https://gitlab.com/gitlab-com/Product/-/issues) and review the drafted Product OKRs with the CEO.

- **3 weeks** prior to the start of the new quarter, the CEO approves the OKRs and the Chief Product Officer shares the OKRs with the [product leadership](/handbook/product/product-leadership/) team for alignment and to finalize ownership of the KRs to specific product team members. 
- **2 weeks** prior to the start of the new quarter, the EBA will port the finalized Chief Product Officer OKRs and KRs from [the Q3 planning issue](https://gitlab.com/gitlab-com/Product/-/issues/2715) approved by Chief Product Officer and VP of Product into [Ally.io](https://app.ally.io/teams/39480/objectives?tab=0&time_period_id=135090). EBA will assign the Objectives and Key Results to the designated owners, as specified by the Chief Product Officer.
     - The EBA will align the Chief Product Officer OKRs to their corresponding parent-level OKRs so everything cascades from the Company Objectives set by the CEO and Chief of Staff.
     - The EBA will link the OKR descriptions in Ally.io to the related GitLab epics/issues for ease of reference to the planning artifacts.
- **1 week** prior to the start of the new quarter, the EBA will tag Product Operations and GMPs to notify them the approved [Product Org Objectives and Key Results](https://gitlab.com/gitlab-com/Product/-/issues/2715) are in Ally. GMPs can then add additional OKRs to Ally that are not already included in the approved Product Org OKRs. **We request that GMPs ensure any OKRs they add do not already exist in Ally.** To learn how to access and use Ally, please check out [how to use Ally.io](#how-to-use-allyio). 

##### OKR Collaboration by the rest of Product Team

**How to update the status of Aligned Product OKRs**

- Team members who **own** specific OKRs will update [the score (% complete) in Ally.io](https://about.gitlab.com/company/okrs/#scoring-okrs) monthly and ping any relevant stakeholders. For example, since FY22-Q3 begins August 1, KR owners will provide score updates by  August 15 September 15 and  October 15. We follow this mid-month cycle to ensure there are accurate OKR status updates for [Key Reviews](https://about.gitlab.com/handbook/key-review/) which typically happen around the 20th of each month.
     - It is recommended to direct link to the monthly update in the GitLab epic/issue in the Ally.io OKR description to maintain a clear reference to the SSOT. 
- For any Product Organization OKRs owned by the Chief Product officer, the EBA to the Chief Product Officer will update the Key Review slides around the 18th of each month based on what is in Ally.io and point to direct links in Ally.io as appropriate.
- For any Aligned Product Group OKRs, the **owners** of the specific KRs will get pinged to update the Key Review slides around the 18th of each month based off of what is in Ally.io and point to direct links in Ally.io as appropraite.

**Note:** OKR owners will get reminders via Slack/Ally.io on or near the 15th of each month to do status updates. To set up more reminders or opt out of reminders, check out [how to use Ally.io](#how-to-use-allyio)   

**How to report progress of Aligned Product OKRs**

- proposal in progress on consistency and accuracy of committed vs aspirational OKRs
     - Need to add outcomes after proposal issue is aligned upon by product and engineering stakeholders
     - It's critical to define how we're scoring progress for Allied Product Group OKRs accurately and consistently across all product groups-- this isn't about how in the tools to score  but rather alignment on what success looks like for a KR



### How to write OKRs

It is highly recommended that product groups limit the number of OKRs they commit to so they have reasonable bandwidth to deliver  on them. PMs should consider either setting 1 OKR that's not related to a product theme **OR** 3 or fewer KRs if they are using the product themes as their objectives. 

Here's are some examples of  how GMPs and PMs can use  epics/issues to plan and outline their product group OKRs:

[Create stage](https://gitlab.com/gitlab-org/create-stage/-/issues/12875)
     - [Create: Editor group](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12044)
     - [Create: Code review group](https://gitlab.com/gitlab-org/create-stage/-/issues/12877)

[Verify stage](https://gitlab.com/gitlab-org/verify-stage/-/issues/89)

To learn more about what OKRs are and the importance of OKRs at GitLab, see the [company OKR handbook page](https://about.gitlab.com/company/okrs/). 

To learn about the industry best practices for OKRs, how setting the right goals can mean the difference between success and failure, and how we can use OKRs to hold our leaders and ourselves accountable, watch [John Doerr's Ted Talk](https://www.youtube.com/watch?v=L4N1q4RNi9I). You can also find more resources in [Product Management Learning & Development](https://about.gitlab.com/handbook/product/product-manager-role/learning-and-development/#-kpis-and-metrics)

For information on [getting started with OKRs](https://learn.ally.io/path/employee/getting-started-with-okrs) and [writing basic okrs](https://learn.ally.io/writing-basic-okrs) see the linked courses from our OKR vendor Ally.

Samples of well written KRs from GitLab team members from the [Q2 product group KR experiment](https://gitlab.com/gitlab-com/Product/-/issues/2095). What makes these great is that they're succinct in scope and have clear metrics to measure success. 

**Product theme (Objective)**: Drive a meaningful impact on Usability (Bugs, Infradev, Security) 
**KRs (Key Results):**
- group::threat insights: Meet SLAs for all P1 and P2 bugs affecting usability
- group::code review: Reduce mean-time-to-close of S1 + S2 bugs by 50%
- group::editor: Complete 10 usability issues related to our primary categories (Web IDE, Snippets, Wiki)

### How to use Ally.io

For an overview of how Product is currently using Ally.io and how to do some basic tasks, check out this [overview video](https://youtu.be/hP9yk_PSj2k). We also have additional training materials provided by Ally.io and Gitlab in [this issue](https://gitlab.com/gitlab-com/ally-okr-rollout/-/issues/9) We recommend you check out everything in this issue but these are some specific links for common topics:

- [How to access Ally.io](https://gitlab.com/gitlab-com/ally-okr-rollout/-/issues/9#how-to-access-ally)
- [How Ally.io and Slack currently work together](https://gitlab.com/gitlab-com/ally-okr-rollout/-/issues/9#slack-integration)
- [How Ally.io and GitLab currently work together](https://gitlab.com/gitlab-com/ally-okr-rollout/-/issues/9#ally-and-gitlab-integration) 
- [How to change the scoring and do check-ins of an OKR in Ally.io](https://gitlab.com/gitlab-com/ally-okr-rollout/-/issues/9#what-are-check-ins)
- [How to update the various details or descriptions of an OKR in Ally.io](https://gitlab.com/gitlab-com/ally-okr-rollout/-/issues/9#how-to-create-okrs)

### Product OKRs by Quarter

- [Product OKRs FY22-Q2 (active)](https://gitlab.com/gitlab-com/Product/-/issues/2376)
- [Product OKRs FY22-Q1](https://gitlab.com/gitlab-com/Product/-/issues/1914)
- [Product OKRs FY21-Q4](https://gitlab.com/gitlab-com/Product/-/issues/1566)
- [Product OKRs FY21-Q3](https://gitlab.com/gitlab-com/Product/-/issues/1320)

## How to contribute to this page

We encourage suggestions and improvements to the Product OKR process so please feel free to raise an MR to this page. To keep us all aligned, as the process touches all teams across the company, please assign all MRs for collaboration, review and approval to [Product Operations](https://gitlab.com/fseifoddini) and the [EBA to the Chief Product Officer](https://gitlab.com/kristie.thomas). 

